This crate provides common types for 2D geometric algebra in Rust.

It includes the following key types:

- Vectors (`Vector`)
- Bivectors (`Bivector`)
- Rotors (`Rotor`)

The crate aims to offer a concise and efficient implementation of 2D geometric algebra, making it suitable for use in game development, mathematical applications, and other domains that require geometric algebra.

## Features

- Provides a simple and intuitive API for working with 2D geometric algebra types
- Supports basic arithmetic operations (addition, subtraction, multiplication, division) on vectors, bivectors, and rotors
- Implements common mathematical operations like dot product, inner product, and outer product
- Offers convenient methods for creating and manipulating vectors, bivectors, and rotors
- Supports serialization and deserialization of geometric algebra types using the `data-stream` crate (enabled by the `data-stream` feature)
- Allows parsing and formatting of geometric algebra types using the `token-parser` crate (enabled by the `parsable` feature)
