use crate::Vector;
use token_parser::{Context as ParseContext, Parsable, Parser, Result as ParseResult};

impl<C: ParseContext, T: Parsable<C>> Parsable<C> for Vector<T> {
    fn parse_list(parser: &mut Parser, context: &C) -> ParseResult<Self> {
        Ok(Self {
            x: parser.parse_next(context)?,
            y: parser.parse_next(context)?,
        })
    }
}
