#![deny(missing_docs)]

/*!
This is a crate for 2D geometric algebra.

It only contains the most important types:

- vectors
- bivectors
- rotors
*/

mod bivector;
mod rotor;
mod vector;

pub use bivector::Bivector;
pub use rotor::Rotor;
pub use vector::Vector;

#[cfg(feature = "data-stream")]
mod data_stream;
#[cfg(feature = "parsable")]
mod parsable;
